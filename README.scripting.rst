Using Pipe Dream for shell scripting
====================================

Ok, first of all: this is a really dumb idea.
If you need a shell, use a shell. Most of them will be better
suited for the job then stupid old Javascript running in a V8 runtime.

And even if you decide to use Javasript, have a look at something like `zx`_ or
one of its gazillion competitors. They are all propbably better then Pipe
Dream.

.. _zx: https://github.com/google/zx

I just wanted to try something a little differently.


Concepts
========

It's all about pipes & filters, right?

Input and Output are all just `Readable` streams. We use those as our "pipes".
Let's create some input

.. code:: coffee

  input = Readable.from [
    """
    GET /index.html HTTP/1.1
    Host: google.de
    """
  ]

Filters are programs that consume input and produce output.  The most obvious
example is a filter that spawns a child process.  Let's create a filter this
way:

.. code:: coffee

  filter = spawn "telnet", "google.de", "80"

Note that the `telnet` command is not executed yet. For that, we will have to
actually *apply* the filter to some input, or more precisely to an *execution
environment*.

.. code:: coffee

  proc = filter mk_env(input)

In our model, a *filter* is a function that takes an *execution environment*
and produces a *process*. The *execution environment* is some algebraic type
that holds the input to be consumed by the filter aswell as probably some
environment variables and stuff. Not decided yet.

A *process* is another algebraic type. It encompasses two `Readable` streams
``error`` and ``output``.  Upon termination, it yields a *result*, which is yet
another tupel. It contains the *exit status* and the remaining input that was
not consumed by the process, i.e. another `Readable`.

.. code:: coffee

  Promise.all [
    proc.result          # eventually yields exit status and remaining input
    collect(proc.output) # eventually yields all output messages
    collect(proc.error)  # eventually yields all error messages
  ].then ([{exitStatus, remainingInput}, outMessages, errMessages]) ->
    # (do interesting stuff with the result)

This seems complicated. Why are you telling me all this?
========================================================

Using the concepts described above, it is easy to define a couple of
monadic shell combinators. The two most obvious ones would be
the equivalent of the Unix shell's `&&` and `|` respectively.

``.then``
---------

.. code:: coffee

  proc.then (env)-> create_another_process(...)

This would start the second process after the first has exitted with zero exit
status.  Remaining input of the first process would be used as input for the
second.  The output and error streams of both processes would be concatenated
respectively.  The result of the second process would be the result of the
combined process.  All in all this would be the equivalent of the `&&`
combinator in a unix shell.  It would only make sense to implement then in a
Promise/A+ conformant way.

``.pipe``
---------

.. code:: coffee

  proc.pipe (env)-> create_another_process(...)

This would be the equivalent of the unix shell's `|` combinator.  Both
processes would run concurrently.  The output of the first process would be
used as input for the second.  The error streams of both processes would be
merged/interleaved.  The result of the combined process would be available when
both processes have exited. It would reflect the exitCode and remainingInput of
the second process.
