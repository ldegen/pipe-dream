scan = require "../src/scan"
fromArray = require "../src/from-array"
collect = require "../src/collect"
describe "scan", ->
  it "folds a stream over a binary operation, emitting intermediate results", ->
    collect scan fromArray([1,2,3]),0,(a,b)->a+b
      .then (chunks)->
        expect(chunks).to.eql [0,1,3,6]

  it "emits the initial result if the input stream ends before any events have arrived", ->
    collect scan fromArray([]),0,(a,b)->a+b
      .then (chunks)->
        expect(chunks).to.eql [0]
