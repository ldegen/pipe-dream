Clock = require "../src/clock"
Promise = require "bluebird"
describe "A Clock", ->
  it "can schedule tasks in a controlled order", ->
    trace = []
    clock = Clock()

    # the first argument of schedule should be a function that takes the
    # current tick when schedule is called and produces a temporal predicate,
    # i.e. a function that is called for every subsequent tick and if the task
    # should be called for that tick Here are some examples

    # true for exactly one tick given by target
    at = (target)->(now)->(t)->t is target

    # true for every n-th tick starting now
    every = (n)->(now)->(t)->((t-now) % n) is 0

    # true for exactly one tick that is k ticks from now
    # I want to call it in, but in is a keyword
    in_ = (k)->(now)->(t)->t is now + k

    clock.schedule at(5),    (t)->trace.push "it's #{t} o'clock"
    clock.schedule every(2), (t)->trace.push "tick #{t}"

    # run for ticks 0,1,2,3,4
    clock.run (t)->t<5
      .then (t)->
        expect(t).to.eql 5
        expect(trace).to.eql [
          "tick 0"
          "tick 2"
          "tick 4"
        ]
        clock.schedule in_(2), (t)->Promise.delay(10).then ->trace.push "now it's #{t}"
        clock.run (t)->t<10
      .then (t)->
        expect(t).to.eql 10
        expect(trace).to.eql [
          "tick 0"
          "tick 2"
          "tick 4"
          "it's 5 o'clock"
          "tick 6"
          "now it's 7"
          "tick 8"
        ]
