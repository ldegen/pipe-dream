spawn = require "../src/spawn"
map = require "../src/map"
collect = require "../src/collect"
{writeSync} = require "fs"
{Readable} = require "stream"
tmp = require "tmp-promise"
describe "spawn command", ->
  tmpObj = undefined
  beforeEach ->
    tmpObj = tmp.fileSync(keep:false)

  afterEach ->
    tmpObj.removeCallback()

  it "creates a filter by spawning a child process", ->
    api=require.resolve ".."
    writeSync tmpObj.fd, """
      const {wrap,chop,join} = require("#{api}");
      wrap(chop.lines(process.stdin))
        .map((s)=>Number(s))
        .fold(0,(a,b)=>a+b)
        .then((sum)=>{
          console.log("Sum:",sum);
        });
    """

    input = Readable.from [
      """
      2
      4
      7
      12
      4
      """
    ]
    filter = spawn process.execPath, tmpObj.name
    proc = filter {input}
    p=Promise.all [
      proc.result.then (r)->r.exitStatus
      proc.result.then (r)->collect(r.remainingInput)
      collect(map(proc.output, (b)->b.toString()))
      collect(proc.error)
    ]
    expect(p).to.eventually.eql [
      0
      []
      ["Sum: 29\n"]
      []
    ]

