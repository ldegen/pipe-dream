collect = require "../src/collect"
fromArray = require "../src/from-array"
unit = require "../src/unit"

describe "fromArray", ->
  it "turns an array into a readable", ->
    collect fromArray [1,2,3]
      .then (chunks)->
        expect(chunks).to.eql [1,2,3]

describe "unit", ->
  it "wraps a single value into a stream", ->
    collect unit 42
      .then (chunks)->
        expect(chunks).to.eql [42]
