NormalizeArgs = require "../src/normalize-args"

{PassThrough} = require "stream"


describe "normalizeArgs", ->
  describe "operator calling conventions", ->

    specify "First argument is always the readable we operate on", ->
      r = new PassThrough()
      {input} = NormalizeArgs() r
      expect(input).to.equal r

    specify "may also be something we can unwrap", ->
      r = readable:new PassThrough()
      {input} = NormalizeArgs() r
      expect(input).to.equal r.readable

    specify "...but no funny stuff", ->
      r = {foo:"bar"}
      expect(->NormalizeArgs() r).to.throw "cannot get Readable from this"

    specify "mandatory arguments can be given as positional parameters", ->
      {options}=NormalizeArgs(mandatory:["foo","bar"]) new PassThrough, 42, 23
      expect(options).to.eql foo:42,bar:23

    specify "...or as options", ->
      {options}=NormalizeArgs(mandatory:["foo","bar"]) new PassThrough, foo: 42, bar: 23
      expect(options).to.eql foo:42,bar:23

    specify "...but not both", ->
      call = NormalizeArgs mandatory:["foo","bar"]
      expect(->call new PassThrough, foo: 42, 23).to.throw "no further positional arguments allowed"

    specify "number of positional arguments is checked", ->
      call = NormalizeArgs mandatory:["foo","bar"]
      expect(->call new PassThrough, 42, 23, "oink").to.throw "incorrect number of positional arguments.
                                                               Expected 2 but got 3"

    specify "mandatory arguments must not be null or undefined", ->
      call = NormalizeArgs mandatory:["foo","bar"]
      expect(->call new PassThrough, foo:42).to.throw "missing required option bar"
      expect(->call new PassThrough, null, 23).to.throw "missing required option foo"

  describe "A generic check", ->
    it "can conveniently wrap a given operator implementation", ->
      trace=[]
      op = (input, options)->
        trace.push {input,options}
      wrappedOp = NormalizeArgs mandatory:["foo","bar"], wrap: op
      r = new PassThrough
      wrappedOp r, 42,23
      expect(trace.length).to.eql 1
      expect(trace[0].input).to.equal r
      expect(trace[0].options).to.eql foo:42,bar:23
