filter = require "../src/filter"
Clock = require "../src/clock"
Sequence = require "../src/sequence"

describe "filter", ->
  clock=sequence=unsequence=undefined
  beforeEach ->
    clock = Clock()
    {sequence,unsequence} = Sequence clock, delimiter:'|'

  it "does what you think it does", ->
    input = sequence "1|2|3|4"
    expected =  " |2| |4"
    output = filter input, (x)->x%2 is 0
    expect(unsequence(output)).to.eventually.equal expected
