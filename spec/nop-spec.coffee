nop=require "../src/nop"
{Readable,PassThrough}= require "stream"
collect = require "../src/collect"
Clock = require "../src/clock"
{at} = Clock
describe "nop", ->

  # programs together with the ``then`` operator as "bind"
  # and ``nop`` as "unit" form a monad.

  #  - produces no output or error
  #  - terminates at once with zero exit status
  #  - consumes no input
  it "produces no output or error", ->

    env = input: Readable.from [1,2,3]
    proc = nop env

    p=Promise.all [
      collect(proc.output)
      collect(proc.error)
    ]
    expect(p).to.eventually.eql [
      []
      []
    ]

  it "terminates successfuly at once", ->
    input = new PassThrough objectMode:true
    proc = nop {input}
    # "at once" for our purposes simply means without depending on anything
    # happening with the input.
    proc.result.then ({exitStatus, remainingInput})->
      # "at once" for our purposes simply means without depending on anything
      # happening with the input. At this point in time no input has been scheduled yet.
      # But we already have an exit code:
      expect(exitStatus).to.equal 0

      # Now, lets schedule some input. Can be synchronously, or a bit later, does not matter
      # in the slightest.
      Promise
        .delay(5).then -> input.write "bam!"
        .delay(5).then -> input.end()

      # in fact, remainingInput should be the same stream as input
      expect(remainingInput).to.equal input
      expect(collect(remainingInput)).to.eventually.eql ["bam!"]

