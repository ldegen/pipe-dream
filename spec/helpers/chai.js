var chai = require('chai');
var asPromised = require('chai-as-promised');
var almost = require('chai-almost');
var util = require('util');

chai.config.includeStack = true;
chai.use(asPromised);
chai.use(almost());

global.debuglog = util.debuglog("spec");
global.expect = chai.expect;
global.AssertionError = chai.AssertionError;
global.Assertion = chai.Assertion;
global.assert = chai.assert;
global.Promise = require("bluebird");

global.defer = function() {
  var resolve, reject;
  var promise = new Promise(function(res, rej) {
    resolve = res;
    reject = rej;
  });
  return {
    resolve: resolve,
    reject: reject,
    promise: promise
  };
};


