Promise = require "bluebird"
thru=require "../src/thru"
{Readable,PassThrough}= require "stream"
Clock = require "../src/clock"
{at}=Clock
collect = require "../src/collect"
wait = require "../src/wait"
describe "thru", ->

  it "passes its input on to its output", ->
    env = input: Readable.from [1,2,3]
    proc = thru env

    p=Promise.all [
      proc.result.then (r)->r.exitStatus
      proc.result.then (r)->collect(r.remainingInput)
      collect(proc.output)
      collect(proc.error)
    ]
    expect(p).to.eventually.eql [
      0
      []
      [1,2,3]
      []
    ]

  it "does not terminate before all input is consumed", ->
    clock = Clock()
    input = new PassThrough objectMode: true

    trace=[]

    expect(input.readable).to.be.true
    clock.schedule at(5), ->
      input.write "bam!"
    clock.schedule at(10), ->
      input.end()

    # let's do a race :-)
    # (I know who will win.)
    p1 = wait(input).then ->
      trace.push "input ended"
    p2 = thru({input}).result.then ->
      trace.push "thru terminated"

    clock.run ->input.readable
    input.resume()
    Promise.all([p1,p2]).then ->
      expect(trace).to.eql [
        "input ended"
        "thru terminated"
      ]
