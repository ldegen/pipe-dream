{Writable, PassThrough} = require "stream"
Promise = require "bluebird"
util = require "util"
debuglog = util.debuglog "stream.test"

describe "nodejs stream api", ->
  specify "simple Writable that always calls done synchronously", ->
    trace = []
    sink = new Writable
      objectMode: true
      highWaterMark: 0
      write: (chunk, enc, done)->
        debuglog "write called with", chunk
        trace.push "write "+chunk
        done()

    sink.on "drain", ->trace.push "drain"

    # since we set the highWaterMark to zero, the write method
    # will always return false. It will consume all the chunks, though
    # and fire a drain event on the next tick or so.
    # The readableLength should always be zero.
    expect(sink.writableLength).to.equal 0
    expect(sink.write "foo").to.be.false
    Promise.delay(0).then ->
      expect(sink.write "bar").to.be.false
      expect(sink.write "baz").to.be.false
      expect(sink.writableLength).to.equal 0
    .delay(0).then ->
      expect(trace).to.eql [
        "write foo"
        "drain"
        "write bar"
        "write baz"
        "drain"
      ]

  specify "a clogged Writable", ->
    callbacks = []
    consume = ->
      done = callbacks.shift()
      done()
    trace = []
    sink = new Writable
      objectMode: true
      highWaterMark: 0
      write: (chunk, enc, done)->
        debuglog "write called with", chunk
        trace.push "write "+chunk
        callbacks.push done

    sink.on "drain", ->trace.push "drain"

    expect(sink.writableLength).to.equal 0
    expect(sink.write "foo").to.be.false
    # the chunk has been processed, but the
    # callback has not yet been called. So as far as the
    # Writable can tell the chunk has not yet been consumed.
    # So the writableLength is at 1
    expect(sink.writableLength).to.equal 1
    expect(trace).to.eql [
      "write foo"
    ]
    expect(sink.writableBuffer.length).to.equal 0
    # now we write a second chunk, but the first one has not yet been consumed.
    # The Writeable will not call our _write implementation, instead it will buffer
    # the chunk. of course the writableLength will be incremented.
    expect(sink.write "bar").to.be.false
    expect(sink.writableLength).to.equal 2
    expect(trace).to.eql [
      "write foo"
    ]
    expect(sink.writableBuffer.length).to.equal 1
    # next we consume the first chunk, that is, we call the first callback.
    # The second chunk "bar" should be removed from the internal buffer and passed into
    # our _write implementation.
    # Again, we do not call the done callback yet, so as far as the Writable is concerned
    # it has not yet been consumed. The writableLength should thus be 1.
    consume()
    expect(sink.writableLength).to.equal 1
    expect(trace).to.eql [
      "write foo"
      "write bar"
    ]
    expect(sink.writableBuffer.length).to.equal 0
    # Finally, we call the remaining done callback to tell the Writable that we
    # consumed the chunk "bar". The writableLength should go back to zero and
    # the stream should fire a "drain" event.
    consume()
    expect(trace).to.eql [
      "write foo"
      "write bar"
      "drain"
    ]
    expect(sink.writableLength).to.equal 0

  specify "funny PassThrough", ->

    p = new PassThrough objectMode:true, highWaterMark:0
    trace = []
    p.on "data", (d)->trace.push "data "+d
    p.pause()
    p.write "foo"
    expect(trace).to.eql []
    Promise.delay(5).then ->
      expect(trace).to.eql []
      p.resume()
      expect(trace).to.eql []
    .delay(5).then ->
      expect(trace).to.eql ["data foo"]
