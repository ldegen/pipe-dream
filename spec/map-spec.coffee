{Readable} = require "stream"
map=require "../src/map"
Clock = require "../src/clock"
Sequence = require "../src/sequence"
Promise = require "bluebird"
collect = require "../src/collect"

describe "map", ->
  sequence=unsequence=clock=undefined

  beforeEach ->
    clock = Clock()
    {sequence,unsequence} = Sequence clock, delimiter:"|"

  it "does what you think it does", ->
    input = sequence "a|b|c"
    expected =  "A|B|C"
    output = map input, (s)->s.toUpperCase()
    expect(unsequence(output)).to.eventually.equal expected

  it "accepts functions that return promises", ->
    input = sequence "a|b|c"
    expected =  "A|B|C"
    output = map input, (s)->Promise.resolve s.toUpperCase()
    expect(unsequence(output)).to.eventually.equal expected

  it "resolves incoming promises for you", ->
    input = Readable.from ["a","b","c"].map Promise.resolve
    output = map input, (s)->s.toUpperCase()
    expect(collect(output)).to.eventually.eql ["A", "B","C"]
