{PassThrough,Readable} = require "stream"
Promise = require "bluebird"
collect = require "../src/collect"
Clock = require "../src/clock"


describe "The `collect` function", ->
  it "returns a promise of an array containing all items of the stream", ->
    r=Readable.from [1,2,3]
    expect(collect(r)).to.eventually.eql [1,2,3]

  it "rejects the promise if the readable emits an error", ->
    r=Readable.from do ->
      yield 1
      yield 2
      # this should cause the stream to emit an error
      # which in turn should cause the promise returned
      # by collect to be rejected
      throw new Error "Trallalla"

    expect(collect(r)).to.be.rejectedWith "Trallalla"

  it "resolves at once if the stream already ended", ->
    r=Readable.from ["42"]
    collect(r).then ->
      # ignore the result (should be ["42"]).
      # Instead, try to call collect again on the same stream.
      expect(collect(r)).to.eventually.eql []

  it "resumes a paused stream", ->
    r=Readable.from ["42"]
    r.pause()
    expect(r.readable).to.be.true
    expect(r.readableFlowing).to.be.false
    expect(r.isPaused()).to.be.true
    expect(collect(r)).to.eventually.eql ["42"]

  it "can observe all kinds of events", ->
    r = new PassThrough objectMode:true
    clock = Clock()
    at=Clock.at
    clock.schedule at(4), ->r.write "foo"
    clock.schedule at(6), ->r.write "bar"
    clock.schedule at(8),->r.end()
    clock.run (t)->t<10
    actual = collect r,
        getTime:clock.getTime
        events: ["pause","resume","end","data"]
        stringify: true
    expect(actual).to.eventually.eql [
      "0 resume"
      "4 data foo"
      "6 data bar"
      "8 end"
    ]
