chain = require "../src/then"
{Readable} = require "stream"
nop = require "../src/nop"
collect = require "../src/collect"
MockProc = require "../src/moc-proc"


describe "then", ->
  it "executes programms in sequence", ->
    input = Readable.from [
      "foo"
      "bar"
      "baz"
      "bang"
    ]
    trace=[]
    ucProc =  MockProc namePrefix:"uc", trace:trace, transform: (s)->s.toUpperCase()
    proc=[ucProc,ucProc].reduce chain, nop {input}
    Promise
      .all [
        collect(proc.output)
        collect(proc.error)
        proc.result
      ]
      .then ([output,error,result])->
        expect(output).to.eql ["FOO","BAR"]
        expect(error).to.eql [3,3]
        expect(result.exitStatus).to.equal 0
        expect(result.remainingInput.readable).to.be.true
        expect(result.remainingInput.readableFlowing).to.be.false
        expect(result.remainingInput.isPaused()).to.be.true
        expect(trace).to.eql [
          "uc-0 starting"
          "uc-0 started"
          "uc-0 transformed 'foo' to 'FOO'"
          "uc-0 stopping"
          "uc-0 stopped"
          "uc-1 starting"
          "uc-1 started"
          "uc-1 transformed 'bar' to 'BAR'"
          "uc-1 stopping"
          "uc-1 stopped"
        ]
        expect(collect(result.remainingInput)).to.eventually.eql ["baz","bang"]

  it "does not start the second program if the first one fails", ->

    input = Readable.from [
      "foo"
      "bar"
      "baz"
      "bang"
    ]
    trace=[]
    ucFailProc =  MockProc namePrefix:"ucfail", trace:trace, transform: ((s)->s.toUpperCase()), after:->255
    ucProc =  MockProc namePrefix:"uc", trace:trace, transform: (s)->s.toUpperCase()
    proc=[ucFailProc,ucProc].reduce chain, nop {input}
    Promise
      .all [
        collect(proc.output)
        collect(proc.error)
        proc.result
      ]
      .then ([output,error,result])->
        expect(output).to.eql ["FOO"]
        expect(error).to.eql [3]
        expect(result.exitStatus).to.equal 255
        expect(result.remainingInput.readable).to.be.true
        expect(result.remainingInput.readableFlowing).to.be.false
        expect(result.remainingInput.isPaused()).to.be.true
        expect(trace).to.eql [
          "ucfail-0 starting"
          "ucfail-0 started"
          "ucfail-0 transformed 'foo' to 'FOO'"
          "ucfail-0 stopping"
          "ucfail-0 stopped"
        ]
        expect(collect(result.remainingInput)).to.eventually.eql ["bar","baz","bang"]

