take = require "../src/take"
Clock = require "../src/clock"
Sequence = require "../src/sequence"
Promise = require "bluebird"
collect = require "../src/collect"
describe "take", ->
  it "takes the first n elements of a stream", ->
    clock = Clock()
    {sequence,unsequence} = Sequence clock, delimiter:'|'

    input = sequence "a|b| | |c|d"
    expected =  "a|b| | |c"
    #input.on "data", (c)->console.log "input data",c
    #input.on "end", ->console.log "input end"
    #input.on "error", (err)->console.log "input err",err
    output = take input, 3
    unsequence(output).then (actual)->
      expect(actual).to.equal expected
      expect(input.isPaused()).to.be.true
      input.resume()

      expect(input.isPaused()).to.be.false
      expect(input.readableFlowing).to.be.true
      expect(input.readable).to.be.true
      collectedInput = collect(input)
      clock.run ->input.readable
        .then (t)->
          expect(collectedInput).to.eventually.eql ["d"]

      #expect(unsequence(input)).to.eventually.eql "d"

  it "ends the output stream if the input ends prematurely", ->
    clock = Clock()
    {sequence,unsequence} = Sequence clock, delimiter:'|'

    input = sequence "a|b| "
    expected =  "a|b| "
    output = take input, 3
    expect(unsequence output).to.eventually.eql expected
