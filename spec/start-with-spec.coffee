startWith = require "../src/start-with.coffee"
fromArray = require "../src/from-array.coffee"
collect = require "../src/collect"
{Readable} = require "stream"
describe "startWith", ->
  it "works like cons, only for streams", ->
    r = Readable.from [1,2,3]
    collect startWith r, 0
      .then (chunks)->
        expect(chunks).to.eql [0,1,2,3]
