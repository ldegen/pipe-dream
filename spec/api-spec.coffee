{flatmap, fromArray, fold, collect, wrap} = require "../src/api"

describe "API usage examples", ->
  specify "basic usage", ->
    readable = flatmap(fromArray([1,2,3]), ((d)->fromArray([-d,d*d])))
    expect(collect(readable)).to.eventually.eql [-1,1,-2,4,-3,9]
    expect(fold(readable,0,(a,b)->a+b)).to.eventually.eql 8

  specify "syntax sugar", ->
    readable = wrap fromArray [1,2,3]
      .flatmap (d)->fromArray [-d,d*d]
    expect(readable.collect()).to.eventually.eql [-1,1,-2,4,-3,9]
    expect(readable.fold 0, (a,b)->a+b).to.eventually.eql 8

