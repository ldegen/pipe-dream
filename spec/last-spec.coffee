last = require "../src/last.coffee"
Clock = require "../src/clock"
Sequence = require "../src/sequence"

describe "last", ->
  it "skips all but the last chunk of its input", ->
    clock = Clock()
    {sequence,unsequence} = Sequence clock, delimiter:'|'
    # seems we cannot get rid of the extra tick delay at the end.
    # It doesn't matter as long as it is *always* there.
    input = sequence "1|2|3|4|5|6|7|8"
    expected =  " | | | | | | | |8"
    expect(unsequence(last input)).to.eventually.eql expected

