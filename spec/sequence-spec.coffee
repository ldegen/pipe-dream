Clock = require "../src/clock"
{at} = Clock
{PassThrough} = require "stream"
Promise = require "bluebird"
Sequence = require "../src/sequence"
collect = require "../src/collect"
describe "sequence", ->
  clock=undefined
  observe = undefined
  beforeEach ->
    clock = Clock()

  it "creates a timed sequence of events from a simple string", ->
    { sequence } = Sequence clock

    # events are tokens delimited by whitespace.
    # The time an event is scheduled is determined by the start position
    # of the word within the string
    stream = sequence "foo bar    baz   "
    actual = collect stream, events:["data","end","error"], stringify: true, getTime: clock.getTime
    clock.run ->stream.readable
      .then ->
        expect(actual).to.eventually.eql [
          "0 data foo"
          "4 data bar"
          "11 data baz"
          "17 end"
        ]


  it "supports an alternative syntax using delimiters", ->
    { sequence } = Sequence clock, delimiter:"|"

    # alternatively we can define a separator, which allows us to excert more control
    # on the actual clock ticks
    stream = sequence 'a | |b ||  |c '
    actual = collect stream, events:["data","end","error"], stringify: true, getTime: clock.getTime
    clock.run ->stream.readable
      .then ->
        expect(actual).to.eventually.eql [
          "0 data a"
          "2 data b"
          "5 data c"
          "6 end"
        ]
describe "unsequence", ->
  clock=undefined
  observe = undefined
  beforeEach ->
    clock = Clock()
  it "watches a stream and describes it as a string", ->

    # this is particulary useful when asserting post conditions in unit tests.
    stream = new PassThrough objectMode: true
    clock.schedule at(0), ->stream.write "a"
    clock.schedule at(2), ->stream.write "b"
    clock.schedule at(5), ->stream.write "c"
    clock.schedule at(7), ->stream.end()

    {unsequence} = Sequence clock

    # note that unsequence will run the clock until the stream ended.
    expect(unsequence(stream)).to.eventually.eql "a b  c "

  it "will raise an exception if the sequence cannot be represented using the simple syntax", ->

    # this is particulary useful when asserting post conditions in unit tests.
    stream = new PassThrough objectMode: true
    clock.schedule at(0), ->stream.write "aaaaa"
    clock.schedule at(2), ->stream.write "b"
    clock.schedule at(5), ->stream.write "c"
    clock.schedule at(7), ->stream.end()

    {unsequence} = Sequence clock

    # note that unsequence will run the clock until the stream ended.
    expect(unsequence(stream)).to.be.rejectedWith "Cannot use the simple syntax, since event
      'aaaaa' at time 0 would overlap event 'b' at time 2."

  it "supports the alternative syntax using delimiters", ->

    stream = new PassThrough objectMode: true
    clock.schedule at(0), ->stream.write "aaaaa"
    clock.schedule at(2), ->stream.write "b"
    clock.schedule at(5), ->stream.write "c"
    clock.schedule at(7), ->stream.end()

    {unsequence} = Sequence clock, delimiter:"|"

    expect(unsequence(stream)).to.eventually.eql "aaaaa| |b| | |c| "

  it "allows specifying a minimum cell width", ->

    stream = new PassThrough objectMode: true
    clock.schedule at(0), ->stream.write "aaaaa"
    clock.schedule at(2), ->stream.write "b"
    clock.schedule at(2), ->stream.write "c"
    clock.schedule at(5), ->stream.write "d"
    clock.schedule at(7), ->stream.end()

    {unsequence} = Sequence clock, delimiter:"|", minWidth: 2

    expect(unsequence(stream)).to.eventually.eql "aaaaa|  |b c|  |  |d |  "

describe "sequence/unsequence roundtrip", ->
  it "works with simple syntax", ->
    clock = Clock()
    {sequence,unsequence} = Sequence clock
    spec = "a b  c "
    stream = sequence(spec)
    expect(unsequence(sequence(spec))).to.eventually.equal spec
  it "works with delimiters", ->
    clock = Clock()
    {sequence,unsequence} = Sequence clock, delimiter:"|", minWidth: 1
    spec = "a| |b| |c| "
    stream = sequence(spec)
    Promise.all( [
      unsequence(stream)
      collect stream, stringify:true, getTime:clock.getTime, events:["end","data","finish","close"]
    ]).then ([spec2, events])->
      #console.log "events", events
      expect(spec2).equal spec

