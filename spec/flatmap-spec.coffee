{Writable, PassThrough, Readable} = require "stream"
flatmap = require "../src/flatmap.coffee"
unit = require "../src/unit"
Clock = require "../src/clock"
{at, every} = Clock
util = require "util"
Promise = require "bluebird"
Sequence=require "../src/sequence"
collect = require "../src/collect"
describe "flatmap", ->

  input = undefined
  source = undefined
  sink=undefined
  clock=undefined
  debuglog=undefined
  trace=undefined
  sequence=undefined
  unsequence=undefined

  beforeEach ->
    clock = Clock()
    {sequence, unsequence} = Sequence clock, delimiter:'|', minWidth: 2
    sources = {}
    source = (x,val)->
      if val?
        sources[x]=val
      else if not sources[x]?
        s= new PassThrough highWaterMark:0, objectMode:true
        sources[x]=s
      sources[x]
    debuglog0 = util.debuglog "flatmap.test"
    debuglog = (args...)->debuglog0 "t=#{clock.getTime()}",args...

  # the most basic flatmap variant, with no concurrency limit.
  # It will faithfully collect *all* chunks from *all* streams.
  # In real-life this will obviously limit its use-cases.

  it "merges *all* chunks from *all* sources", ->
    input =     sequence "a |b |c    "
    source "a", sequence "a1|  |     |     |  |a2|  |a3|  "
    source "b", sequence "b1|  |     |b2   "
    source "c", sequence "c1|c2|     |c3   "
    expected =      "a1|b1|c1 c2|b2 c3|  |a2|  |a3|  "
    output = flatmap input, source
    expect(unsequence(output)).to.eventually.eql expected

  it "propagates backpressure to sources", ->

    trace = []
    input = new PassThrough highWaterMark:0,objectMode:true
    sink = flatmap input,
      inletOptions:
        highWaterMark:0
      sinkOptions:
        highWaterMark:0
      f: source

    sink.on "data", (d)->
      trace.push d
    # when the buffer of the sink stream runs full,
    # we expect one thing to happen for sure.
    # Backpressure will naturally propage to the source streams, since they are
    # directly connected to the sink via pipes. So if we set the highWaterMark to 0 everywhere,
    # pausing the sink should cause all sources to pause.


    clock.schedule at(2), ->
      input.write "a"
      input.write "b"
    clock.schedule at(3), ->
      source("a").write "a-1"
    clock.schedule at(4), ->
      sink.pause()
      source("b").write "b-1"
      source("a").write "a-2"
    clock.run (t)->t<10
      .then ->
        expect(trace).to.eql [
          "a-1"
        ]
        expect(source("b").readableFlowing).to.be.false
        expect(source("a").readableFlowing).to.be.false
        # the input should only be affected indirectly
        # and only if we implement a concurrency limit.
        # (we write another test case for this)
        expect(input.readableFlowing).to.be.true

  it "can behave like flatmapLatest", ->

    input =     sequence 'a |  |b |  |  | c'
    source "a", sequence 'a1|a2|  |  |a3|a4|  '
    source "b", sequence '  |  |  |b1|  |b2|  |b3|  '
    source "c", sequence '  |  |  |  |  |  |c1'
    expected =      'a1|a2|  |b1|  |  |c1'
    output = flatmap input,
      concurrencyLimit: 1
      enforce: (sources)->0
      f: source

    expect(unsequence(output)).to.eventually.eql expected

  it "can behave like flatmapFirst", ->

    input =     sequence 'a |  |b |  |  | c'
    source "a", sequence 'a1|a2'
    source "b", sequence '  |  |  |b1|  |b2|  |b3|  '
    source "c", sequence '  |  |  |  |  |  |c1'
    expected =      'a1|a2|  |  |  |  |c1'
    output = flatmap input,
      concurrencyLimit: 1
      enforce: "discard"
      f: source

    expect(unsequence(output)).to.eventually.eql expected

  it "can behave like flatmapConcat", ->

    input =     sequence 'a |  |b |  |  |c '
    source "a", sequence 'a1|a2|  |  |a3|  '
    source "b", sequence '  |  |  |b1|  |b2|     |b3|  '
    source "c", sequence '  |  |  |  |  |  |c1'
    expected =      'a1|a2|  |  |a3|  |b1 b2|b3|  |c1'
    output = flatmap input,
      concurrencyLimit: 1
      enforce: "defer"
      f: source

    expect(unsequence(output)).to.eventually.eql expected

  it "experiment: how do async generators work?", ->

    input = Readable.from do ->
      yield 1
      yield Promise.delay(5).then -> 2
      yield 3

    expect(collect input).to.eventually.eql [
      1,2,3
    ]

  it "catches and propagates errors thrown during the transform", ->
    input = Readable.from do ->
      yield 1
      yield Promise.delay(5).then -> 2
      yield 3

    promise = collect flatmap input, (x)->
      if x is 2
        throw new Error "oopsie"
      else
        unit x

    expect(promise).to.be.rejectedWith "oopsie"

  it "detects and propagates errors from its input", ->

    inputWithError = Readable.from do ->
      yield 1
      throw new Error "pustekuchen"
      yield 3


    promise = collect flatmap inputWithError, (a)->unit

    expect(promise).to.be.rejectedWith "pustekuchen"

  it "detects and propagates errors from its input (variant)", ->

    inputWithError = new PassThrough objectMode:true
    Promise
      .delay(5).then -> inputWithError.write 1
      .delay(5).then -> inputWithError.destroy new Error "pustekuchen"

    promise = collect flatmap inputWithError, unit

    expect(promise).to.be.rejectedWith "pustekuchen"

  it "detects and propagates errors from any of the sources", ->

    input = Readable.from do ->
      yield 1
      yield Promise.delay(5).then -> 2
      yield 3


    promise = collect flatmap input, (x)->
      if x is 2
        Readable.from do ->
          yield 1
          yield Promise.delay(5).then ->Promise.reject new Error "pustekuchen"
      else
        unit x

    expect(promise).to.be.rejectedWith "pustekuchen"

