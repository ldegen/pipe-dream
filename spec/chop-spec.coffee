{Readable} = require "stream"
chop = require "../src/chop"
collect = require "../src/collect"

describe "The `chop` function", ->
  it "splits a continuos stream into discrete elements", ->

    r=Readable.from [
      "foo bar b"
      "az bang "
      ""
      "   "
      ""
      "bum "
    ], objectMode:false

    # Here we use the most general form of the `chop` function
    # to split a stream into whitespace-delimited words.
    chopWords = chop
      # `combine` updates the buffer with the current chunk
      # e.g. by appending it to the end of the buffer.
      combine: (buf,chunk,enc)->buf+chunk

      # Whenever `combine` is called, and there is no buffer (yet)
      # we call `initialize` to create one.
      initialize: ->""

      # `split` inspects the current state of the buffer and
      # returns an array of elements. All but the last element
      # will be pushed downstream, the last element will become
      # the new buffer. If the last element is `null` or `undefined`, the
      # buffer will be reinitialized before the next call to `combine`.
      split: (buf)->buf.split(/\s+/)

      # Optional: when the input ends, we need to flush the remaining
      # data from the buffer downstream.
      # By default, this will just use the current buffer value
      # and pass it as it is as a single chunk. For our case, this is perfect.
      # But of course if you use some custom data structure as your buffer, this
      # will require some extra processing.
      # You can define your own flush function that is passed the current buffer value
      # and should return a (possibly empty) list of elements to be pushed downstream.
      # If the returned value is undefined or null, it will be treated just like an empty
      # list.
      #
      # flush: (buf) -> [... some elements ...]

      # Optional: for many applications, you want to surpress certain elements.
      # For instance in our case, we are not interested in empty words. Our test case
      # has been engineered to produce a couple of empty words if you remove this filter.
      # Try it and watch the test fail.
      filter: (s)->s.length>0


    expect(collect(chopWords(r))).to.eventually.eql ["foo", "bar", "baz", "bang", "bum"]

  it "emit error events if something goes wrong in `combine`", ->
    r=Readable.from ["42"]
    chopper = chop
      combine: -> throw new Error "combine failed"
      split: ->[]
      initialize: ->""
    expect(collect(chopper(r))).to.be.rejectedWith "combine failed"

  it "emit error events if something goes wrong in `split`", ->
    r=Readable.from ["42"]
    chopper = chop
      combine:(buf)->buf
      split: ->throw new Error "split failed"
      initialize: ->""
    expect(collect(chopper(r))).to.be.rejectedWith "split failed"

  it "emit error events if something goes wrong in `initialize`", ->
    r=Readable.from ["42"]
    chopper = chop
      combine:(buf)->buf
      split: ->[]
      initialize: ->throw new Error "initialize failed"
    expect(collect(chopper(r))).to.be.rejectedWith "initialize failed"

  it "emit error events if something goes wrong in `flush`", ->
    r=Readable.from ["42"]
    chopper = chop
      combine:(buf)->buf
      split: ->[]
      initialize: ->""
      flush: -> throw new Error "flush failed"
    expect(collect(chopper(r))).to.be.rejectedWith "flush failed"

describe "The `chop.lines` function", ->
  it "provides a shortcut for splitting a raw stream into lines", ->
    r=Readable.from [
      """
      Das Schwirren eines aufgeschreckten Sperlings
      begeistert Korf zu """
      """
      einem Kunstgebilde,
      das nur aus Blicken, """
      """Mienen und Gebärden
      besteht. Man kommt mit Apparaten,
      es aufzunehmen; doch v. Korf ›entsinnt sich
      des Werks nicht mehr‹, entsinnt sich keines Werks mehr
      anläßlich eines ›aufgeregten Sperlings‹.
      """
    ], objectMode:false
    expect(collect(chop.lines(r))).to.eventually.eql [
      "Das Schwirren eines aufgeschreckten Sperlings"
      "begeistert Korf zu einem Kunstgebilde,"
      "das nur aus Blicken, Mienen und Gebärden"
      "besteht. Man kommt mit Apparaten,"
      "es aufzunehmen; doch v. Korf ›entsinnt sich"
      "des Werks nicht mehr‹, entsinnt sich keines Werks mehr"
      "anläßlich eines ›aufgeregten Sperlings‹."
    ]
    expect(collect(chop.lines(r,maxLength:10))).to.be.rejectedWith "maximum line length exceeded"

describe "The `chop.nop`function", ->
  it "passes the chunks unchanged", ->
    # ... but it will return a readable stream created with objectMode: true
    # I am not sure if this makes any difference.
    r=Readable.from [
      "foo bar b"
      "az bang "
      ""
      "   "
      ""
      "bum "
    ], objectMode:false

    # without the chop.nop...
    expect(collect(r)).to.eventually.eql [
      "foo bar b"
      "az bang "
      "   "
      "bum "
    ].map (s)->Buffer.from s

    # with the chop.nop
    expect(collect(chop.nop(r))).to.eventually.eql [
      "foo bar b"
      "az bang "
      "   "
      "bum "
    ].map (s)->Buffer.from s


