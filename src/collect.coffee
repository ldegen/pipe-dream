Promise = require "bluebird"
unwrap = require "./unwrap"
wait = require "./wait"
NormalizeArgs = require "./normalize-args"
module.exports = NormalizeArgs wrap:(input, options={})->
  {
    events    # list of events to report. If not set, only data will be reportet
              # If specified, the events of the listed type will be reported.
              # In this case output is wrapped in objects with properties
              # `type`, `args` and optional `time`.
    getTime   # if present, will be called for each chunk to get a timestamp.
              # Like above, this implies that output is wrapped in objects.
    stringify # boolean. set to true to format output in more compact string.
              # I use this in unit tests.
  } = options

  chunks = []
  if events? or getTime?
    # report objects
    events ?= ["data"]
    events.forEach (type)->
      input.on type, (arg)->
        obj = {type}
        if arg?
          obj.arg=arg
        if getTime?
          obj.time=getTime()
        chunks.push obj
  else
    input.on "data", (chunk)->
      #console.log "collect chunk", chunk
      chunks.push chunk

  input.resume()
  wait(input).then ->
    if stringify
      chunks.map ({time,type,arg})->
        [time,type,arg]
          .filter (x)->x?
          .join " "
    else
      chunks
