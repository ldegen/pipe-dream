Promise = require "bluebird"
NormalizeArgs = require "./normalize-args"
{PassThrough} = require "stream"

module.exports = NormalizeArgs mandatory:["limit"], wrap:(input,options)->

  {limit, outputOptions={}}=options
  output = new PassThrough {
    objectMode: true
    highWaterMark:0
    outputOptions...
  }

  count = 0

  #console.log "take limit", limit
  listener = (chunk)->
    #console.log "take", chunk
    count++
    #console.log "count", count
    if count is limit
      input.off "data", listener
      input.unpipe output
      process.nextTick ->output.end()

  input.on "data", listener
  input.pipe output
