unwrap = require "./unwrap"
{Readable} = require "stream"
module.exports = ({mandatory=[], wrap}={})->

  normalize = (input0, args...)->

    # first argument is *always* a Readable stream or something
    # we can unwrap a Readable from.
    input = unwrap input0

    if not input instanceof Readable
      throw new Error "input is not a readable"

    # mandatory contains a list of arguments the operator absolutly needs
    # to work. They can either be given as positional arguments or as named options.
    # Optional arguments can only be given as options. (hence the name, i guess...)
    # Mandatory arguments MUST NOT be objects.
    # If options are present, no positional arguments are allowed.


    if args[0]? and typeof args[0] is "object" and not Array.isArray args[0]
      if args.length > 1
        throw new Error "no further positional arguments allowed if you are using an options object"
      [options] = args
    else
      options = {}
      if mandatory.length isnt args.length
        throw new Error "incorrect number of positional arguments.
                         Expected #{mandatory.length} but got #{args.length}"

      for arg,i in args
        options[mandatory[i]]=arg

    for name in mandatory
      unless options[name]?
        throw new Error "missing required option #{name}"

    {input,options}

  if typeof wrap is "function"
    (input0, args...)->
      {input,options} = normalize input0, args...
      wrap input, options
  else
    normalize
