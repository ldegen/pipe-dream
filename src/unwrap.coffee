{Readable} = require "stream"
module.exports = (M)->
  switch
    when M instanceof Readable then M
    when M.readable? and M.readable instanceof Readable then M.readable
    when typeof M.readable is "function" then M.readable()
    else throw new Error "cannot get Readable from this: #{M}"
