Promise = require "bluebird"
{finished}=require "stream"
NormalizeArgs = require "./normalize-args"

module.exports = NormalizeArgs wrap:(input)->
  if input.readable
    new Promise (resolve,reject)->
      cleanup = finished input, (err)->
        cleanup()
        if err? then reject(err) else resolve()
  else
    Promise.resolve()
