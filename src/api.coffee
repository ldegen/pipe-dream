flatmap = require "./flatmap"
map = require "./map"
filter = require "./filter"
scan = require "./scan"
last = require "./last"
startWith = require "./start-with"
take = require "./take"

unit = require "./unit"
fromArray = require "./from-array"
chop = require "./chop"

collect = require "./collect"
unwrap = require "./unwrap"
wait = require "./wait"

NormalizeArgs = require "./normalize-args"

transforms = {
  flatmap
  flatmapLatest: NormalizeArgs mandatory:["f"], wrap:(input,options)->
    flatmap input, {options..., concurrencyLimit:1, enforce: ->0}
  flatmapFirst: NormalizeArgs mandatory:["f"], wrap:(input,options)->
    flatmap input, {options..., concurrencyLimit:1, enforce: "skip"}
  flatmapConcat: NormalizeArgs mandatory:["f"], wrap:(input,options)->
    flatmap input, {options..., concurrencyLimit:1, enforce: "defer"}
  map
  filter
  scan
  last
  startWith
  take
}

sources = {
  unit
  fromArray
  chop
}

sinks = {
  wait
  collect
  unwrap
  get: unwrap
  pipe: NormalizeArgs mandatory:["destination"], wrap:(input, options)->
    input.pipe destination, options
  fold: NormalizeArgs mandatory:["v0","reducer"], wrap:(input, options)->
    collect last scan input, options
      .then (d)->d?[0]
}



Wrap = require "./wrap"

wrap = Wrap {transforms, sinks, unwrap}
wrapped = (f)->(args...)->wrap f args...

for key,value of sources
  wrap[key]=wrapped value


module.exports = {
  transforms...
  sources...
  sinks...
  wrap
}

