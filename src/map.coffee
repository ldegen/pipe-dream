Promise = require "bluebird"
{Readable,Transform} = require "stream"
NormalizeArgs = require "./normalize-args"
module.exports = NormalizeArgs mandatory:"f", wrap:(input, options)->

  {
    # mandatory, the function to apply
    f
    # optional, options to pass on to the Transform constructor
    transformOptions=objectMode:true
  }= options
  sink = new Transform {
    transformOptions...
    transform: (chunk,enc,cb)->
      self=this
      Promise.resolve chunk
        .then f
        .then (elm)->self.push elm
        .then -> cb()
        .catch cb
  }

  input.pipe sink

