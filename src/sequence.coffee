Promise = require "bluebird"
{PassThrough} = require "stream"
{at} = require "../src/clock"
collect = require "../src/collect"
module.exports=(clock,opts={})->
  {
    # if a delimiter is set, the time of an event
    # will not be determined by its start position
    # in the string, but by the number of delimiters before it.
    delimiter

    # the pattern used for detecting events
    # It must have the global flag set, or an exception will be thrown.
    eventPattern = /[^\s]+/g

    # This is only used by unsequence and only if using a delimiter.
    # It can be used to set a minium cell width to make the output more
    # readable. Think "tab stops".
    minWidth = 1

  }=opts

  parse =(spec)->
    if delimiter?
      cells = spec.split delimiter

      if spec.endsWith delimiter
        throw new Error "Your sequence '#{spec}' ends with a #{delimiter}. This will add an
                         extranous empty cell just before the end of the stream, which is
                         hard to read and usually not what you want. Remove the delimiter or
                         add whitespace to make the extra cell more visible."

      length: cells.length
      chunks: cells
        .map (cell,i)->
          Array
            .from cell.matchAll eventPattern
            .map (match)->
              time:i
              chunk: match[0]
        .reduce ((a,b)->a.concat b),[]
    else
      length: spec.length
      chunks: Array
        .from spec.matchAll eventPattern
        .map (match)->
          time: match.index
          chunk: match[0]

  sequence = (spec)->
    stream = new PassThrough objectMode: true

    { chunks, length } = parse(spec)

    chunks.forEach ({time,chunk})->
      clock.schedule at(time), ->
        #console.log time, "sequence writing chunk", chunk
        stream.write chunk
    clock.schedule at(length), (t)->
      new Promise (resolve,reject)->
        #console.log t,clock.getTime(),"end scheduled"
        stream.end (err)->
          if err?
            reject err
          else
            #console.log t,clock.getTime(),"end confirmed by cb"
            resolve()

    stream

  unparseSimple = (events)->
    output = ""
    for event, i in events
      padLength = event.time-output.length
      if i>0 and padLength <= 0
        prevEvent = events[i-1]
        throw new Error "Cannot use the simple syntax, since event '#{prevEvent.arg}'
          at time #{prevEvent.time} would overlap event '#{if event.type is "end" then "end" else event.arg}'
          at time #{event.time}."
      if padLength > 0
        output += " ".repeat padLength
      if event.type is "data"
        output += event.arg
    output

  unparseDelimiter = (events)->
    output = ""
    prevTime = 0
    startOfCell = 0
    for event, i in events
      # how much time has passed since the last event
      increment = event.time-prevTime

      # for each tick that has passed since the last event,
      # insert a delimiter
      for k in [0...increment]
        # if content does not fill the cell, append
        # some padding whitespace
        cellWidth = output.length - startOfCell
        padWidth = minWidth - cellWidth
        if padWidth > 0
          output += " ".repeat padWidth

        # in our encoding, the end event is positioned right after the end of the string,
        # so to say. We MUST NOT insert a delimiter right before the end, as that would
        # encode another (empty) cell before the end. This would break roundtrips!
        unless event.type is "end" and k is increment - 1
          output += delimiter
        # after we inserted the delimiter, keep note
        # of the start of the new cell
        startOfCell = output.length

      # special case: if the tick increment since the last event is zero
      # we write the current increment to the same cell. Make sure to insert
      # some whitespace inbetween.
      if i>0 and event.type isnt "end" and increment is 0
        output += " "

      if event.type is "data"
        output += event.arg

      prevTime = event.time
    output

  unparse = if delimiter? then unparseDelimiter else unparseSimple

  unsequence = (stream)->
    Promise.all([
      clock.run ->stream.readable
      collect stream, events:["data","end"], getTime:clock.getTime
    ])
      .then ([t,events])->unparse events


  {sequence, unsequence}


