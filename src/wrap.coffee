module.exports = ({transforms={}, sinks={}, unwrap})->
  wrap = (M)->
    self = unwrap M
    methods = {}
    for name,transform of transforms
      do (name, transform)->
        methods[name] = (args...)->wrap transform self, args...

    for name,sink of sinks
      do (name, sink) ->
        methods[name] = (args...)->sink self, args...

    methods

