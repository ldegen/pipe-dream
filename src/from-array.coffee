{Readable} = require "stream"
{deprecate} = require "util"
fromArray=(array, opts={})->Readable.from array
module.exports = deprecate fromArray, "fromArray is deprecated. Please use Readable.from instead"

