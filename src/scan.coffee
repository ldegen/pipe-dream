startWith = require "./start-with"
flatmap = require "./flatmap"
unit = require "./unit"
NormalizeArgs = require "./normalize-args"

module.exports = NormalizeArgs mandatory:["v0","reducer"], wrap:(input, options)->

  {
    # mandatory, initial state
    v0
    # mandatory, reducer function, i.e. a function that takes
    # the current previous state and the next element from the input
    # and returns the next state
    reducer
  }=options


  # this holds our state
  v = v0

  f = (elm)->
    v = reducer v, elm
    unit v

  flatmap startWith(input, v0), {options..., f}
