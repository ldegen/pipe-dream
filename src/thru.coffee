Promise = require "bluebird"
{Readable} = require "stream"
wait = require "./wait"
module.exports = (env)->
  {input} = env
  output: input
  error: Readable.from []
  result: wait(input).then ->
    remainingInput:Readable.from []
    exitStatus: 0
