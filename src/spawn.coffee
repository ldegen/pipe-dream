{ Readable } = require "stream"
{ spawn } = require "child_process"
Promise = require "bluebird"

class NonZeroExitCodeError extends Error
  constructor: (exitCode)->
    super "Non-zero exit code: #{exitCode}"
    @exitCode=exitCode

class SignalReceivedError extends Error
  constructor: (signal)->
    super "child process received #{signal}"
    @signal=signal


module.exports = (args...)->

  opts =if typeof args[0] is "object" then args[0] else commandLine:args

  args = Array::slice.call opts.commandLine
  handleExitCode = opts.handleExitCode ? (result)->
    { exitStatus } = result
    if exitStatus != 0
      throw new NonZeroExitCodeError exitStatus
    result
  executable = args.shift()
  (environment)->
    {input = Readable.from([])} = environment
    cp = spawn executable,args,opts
    input.pipe cp.stdin
    promise = new Promise (resolve,reject)->
      cp.on "error", (e)->
        stderr.end()
        stdout.end()
        reject e
      cp.on "exit", (code,signal)->
        if typeof signal == "string"
          reject new SignalReceivedError signal
        else
          resolve
            exitStatus: code
            remainingInput: input
    proc =
      output: cp.stdout
      error: cp.stderr
      result: promise.then handleExitCode

