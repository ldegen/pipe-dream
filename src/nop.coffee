Promise = require "bluebird"
{Readable} = require "stream"
module.exports = (env)->
  output: Readable.from []
  error: Readable.from []
  result: Promise.resolve(
    exitStatus:0
    remainingInput: env.input
  )

