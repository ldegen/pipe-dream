flatmap = require "./flatmap"
fromArray = require "./from-array"
unit = require "./unit"
NormalizeArgs = require "./normalize-args"
module.exports = NormalizeArgs mandatory:["v0"], wrap: (readable, {v0})->
  flatmap fromArray([unit(v0), readable]), (x)->x
