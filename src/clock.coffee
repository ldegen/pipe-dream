Promise = require "bluebird"

module.exports = (options={})->
  {delay=0}=options
  tasks = []

  now = 0

  reset = (t)->
    now = t

  schedule = (spec, job)->
    tasks.push
      test: spec(now)
      task: job

  step = ->
    ps = tasks
      .filter ({test})->test(now)
      .map ({task})->Promise.resolve(task(now))
    Promise.delay(0)
      .then ->Promise.all(ps)
      .delay(delay)
      .then ->now = now + 1

  run = (cond)->
    if cond(now)
      step().then ->run(cond)
    else
      Promise.resolve(now)

  getTime = ->now
  {schedule, run, reset, step,getTime}

# true for exactly one tick given by target
module.exports.at = (target)->(now)->(t)->t is target

# true for every n-th tick starting now
module.exports.every = (n)->(now)->(t)->((t-now) % n) is 0


