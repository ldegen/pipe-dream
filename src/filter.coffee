NormalizeArgs = require "./normalize-args"
{Transform} = require "stream"

module.exports = NormalizeArgs mandatory:["predicate"], wrap: (input,options)->
  tf = new Transform {
    objectMode:true
    options...
    transform: (chunk,enc,done)->
      if options.predicate chunk
        @push chunk
      done()
  }

  input.pipe tf

