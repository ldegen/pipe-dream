{Readable, Writable, PassThrough} = require "stream"
Promise = require "bluebird"
util = require "util"
debuglog = util.debuglog "flatmap"
unwrap = require "./unwrap"
NormalizeArgs = require "./normalize-args"

module.exports = NormalizeArgs mandatory:["f"], wrap:(input,options)->
  {
    # mandatory: a function that takes a single element and produces a Readable
    f
    # passed through to the sink constructor
    sinkOptions={}

    # passed through to the inlet constructor
    inletOptions={}

    # maximum allowed number of active sources
    concurrencyLimit=null

    # what to do when the limit is reached and a new element
    # arrives at the input
    # "discard":
    #   The item will be discarded. No source will be created.
    # "defer":
    #   Processing of the item will be deferred until the at least
    #   one of the currently active sources is finished.
    # a function:
    #   The returned function will be called with an array containing
    #   the current sources. It should return one of the sources to remove.
    #   it may also return an index into the array.
    #   The function will be called in a tight loop until the
    #   concurrencyLimit is enforced.
    enforce="defer"
  } = options

  # naive, in a good way :-)

  # create a custom writable stream, call it "inlet". We pipe the input into inlet.
  # The inlet can be used to excert backpressure when needed.
  # create a PassThrough stream, call it "sink".
  # When an element is writen to inlet, it spawns a stream using f and pipes it to
  # the sink.
  #

  sink = new PassThrough {
    sinkOptions...
    objectMode: true
  }
  sources = []
  deferredChunks = []

  checkEnd = ->
    # if our inlet has shut down and no active sources are left
    # we can end the sink.
    if inletFinished and deferredChunks.length is 0 and sources.length is 0
      debuglog "flatmap end"
      sink.end()

  removeSource = (source)->
    if Number.isInteger(source)
      source = sources[source]
    # FIXME: is this efficient enough?
    sources = sources.filter (s)->s isnt source
    # the sink should be the only consumer attached to this source
    # does not hurt to unpipe just to be sure
    source.unpipe()

    # When we are within the allowed bounds, check if there is a deferred chunk
    # waiting to be processed.
    Promise.resolve(
      if sources.length < concurrencyLimit and deferredChunks.length > 0
        {chunk,enc, done} = deferredChunks.shift()
        processChunk chunk, enc, done
    ).then checkEnd

  addSource = (source)->
    sources.push source
    # we pass end: false, because we definitely do *not* want
    # the sink only because one of the potentially many has ended.
    source.pipe sink, end: false
    source.once "end", ->
      debuglog "source ended"
      removeSource source
    source.once "error", (err)->
      debuglog "soure error", err
      removeSource source
      sink.destroy err

  processChunk = (chunk, enc, done)->
    if concurrencyLimit and sources.length >= concurrencyLimit
      switch
        when enforce is "discard"
          done()
        when enforce is "defer"
          deferredChunks.push {chunk,enc,done}
        when typeof enforce is "function"
          while sources.length >= concurrencyLimit
            removeSource enforce sources
          # try again
          processChunk chunk, enc, done
        else
          throw new Error "bad value for enforce: #{enforce}"

    else
      Promise
        .resolve(chunk)
        .then (chunk)->
          f chunk
        .then (source)->
          debuglog "proccessing input chunk (addSource)", chunk
          addSource source
        .catch (err)->
          debuglog "error caught processing chunk", chunk
          sink.destroy err
        .finally ->
          done()

  inlet = new Writable {
    inletOptions...
    objectMode: true
    write: processChunk
  }

  # when the inlet emits "finish", we can be sure that not only
  # the input has ended, but also that buffers have been drained.
  inletFinished = false

  inlet.once "finish", ->
    debuglog "inlet finished"
    inletFinished = true
    checkEnd()
  inlet.once "error", (err)->
    debuglog "inlet error", err
    sink.destroy err


  sink.__inlet = inlet

  # note how we *not* pass end: false here. We *want*
  # the inlet to close down when the input ends.
  input.pipe inlet

  # not sure if this is necessary, but it probably does not hurt
  # This will make sure our inlet is destroyed when there is any problem
  # on the input side. Above, we check for this error events on the inlet
  # and destroy the sink if any is detected.
  input.once "error", (err)->
    debuglog "input error", err
    inlet.destroy err

  # return the sink
  sink
