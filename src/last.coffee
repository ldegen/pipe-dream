{Readable, Transform} = require "stream"
unwrap = require "./unwrap"
NormalizeArgs = require "./normalize-args"

module.exports = NormalizeArgs wrap:(input, options={})->
  data = undefined

  tf = new Transform {
    options...
    objectMode: true
    transform: (chunk, enc, done)->
      data = chunk
      done()
    flush: (done)->
      @push data
      done()
  }

  input.pipe tf
