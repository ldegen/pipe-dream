{wrap} = require "../src/api"
{Readable} = require "stream"
identity = (x)->x
module.exports = (p, f)->

  q = p.result.then ({exitStatus,remainingInput})->
    if exitStatus is 0
      #console.log "remainingInput paused", remainingInput.isPaused()
      #remainingInput.resume()
      f {input:remainingInput}
    else
      output: Readable.from []
      error: Readable.from []
      result: Promise.resolve {exitStatus, remainingInput}


  output:
    wrap
      .fromArray [p.output, q.then (q)->q.output]
      .flatmapConcat identity
      .get()
  error:
    wrap
      .fromArray [p.error, q.then (q)->q.error]
      .flatmapConcat identity
      .get()
  result:
    q
      .then (q)->q.result
