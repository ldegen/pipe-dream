{Transform} = require "stream"


chop = (opts)->(readable)->
  for name in ["combine","split","initialize"]
    if typeof opts?[name] isnt "function"
      throw Error "the options passed to `chop` must contain a function `#{name}`."

  filter = opts.filter ? -> true
  flush = opts.flush ? (buf)->[buf] if buf?
  {combine,split,initialize} = opts

  buf = null
  t = new Transform
    objectMode: true
    transform: (chunk, enc, callback)->
      try
        buf = combine (buf ? initialize()), chunk, enc
        [elements...,buf] = split buf
        @push element for element in elements when filter element
      catch e
        callback e
        return
      callback()
    flush: (callback)->
      try
        elements = flush(buf) ? []
        @push element for element in elements when filter element
      catch e
        callback e
        return
      callback()

  readable.pipe t

chop.lines = (readable, opts={})->
  {
    # max buffer length in characters (*not* bytes).
    # If the input contains lines that exceed this limit,
    # an exception will be raised.
    # If you don't want that, you can set the value
    # to something falsy to disable the check altogether.
    maxLength = 4096

    # the pattern for matching line ends
    pattern = /\r?\n/

    # character encoding to assume in case we
    # get chunks as octet buffers
    encoding = "utf8"
  } = opts

  chopper = chop
    initialize: ->""
    combine: (buf, chunk, enc)->
      s = switch
        when typeof chunk is "string" then chunk
        when chunk instanceof Buffer then chunk.toString(encoding)
        else throw new Error "bad chunk type: #{chunk}"

      if buf.length + s.length > maxLength
        throw new Error "maximum line length exceeded"

      buf + s
    split: (buf)->buf.split pattern
    filter: (s)->s.length > 0
  chopper readable

chop.nop = (readable)->
  chopper = chop
    initialize: ->null
    combine: (buf, chunk, enc)->
      switch
        when typeof chunk is "string" then Buffer.from(chunk, enc)
        when chunk instanceof Buffer then chunk
        else throw new Error "bad chunk type: #{chunk}"
    split: (buf)->[buf, null]
  chopper readable


module.exports = chop
