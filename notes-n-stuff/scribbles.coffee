# create some input.
# Input is just a plain old Readable stream.
input = Readable.of [
  """
  GET /index.html HTTP/1.1
  HOST: tarent.de
  """
]

# Create a filter.
# Think of a filter as of a partially evaluated shell command:
# - you pass in the command line
# - ... maybe you also bind parts of the process environment
# but the command is not yet executed, because for that it needs a (potentially empty) input.
telnet = spawn "telnet", "localhost", "80"


# Apply the filter to the input, creating an actual process.
# Only now the command is actually executed. It may consume
# some or all of the provided input stream. It will also produce
# output and error - both are Readable streams and can be accessed
# through the process API.
# Eventually, the process will exit, producing an exit code and the remaining
# input as another Readable.

proc = telnet env(input)

# Note that we decided to "bind" the process environment at the latest possible moment.
# A filter is actually not applied to "the input", but more precisely, to the process environment
# of which the input is a crucual part. It also holds the values of environment variables, and potentially
# other stuff I have not yet decided on.
# I want something like the ``env`` function that allows us to inherit defaults for all this from the
# "surrounding context" - I am not yet clear as to what constitutes this context. The parent process, maybe?
# (the parent would be the process executing *this* script)
# For instance if I do this instead
proc = telnet env()
# I create a process environment that copies fd0 from its parent, i.e. I connected telnet
# to the stdin of the process executing this script.

Promise.all [
  proc.result          # eventually yields exit status and remaining input
  collect(proc.output) # eventually yields all output messages
  collect(proc.error)  # eventually yields all error messages
].then ([{exitCode, remainingInput}, outMessages, errMessages]) ->
  # (do interesting stuff with the result)


# By the way: a "filter" is just a function that takes an execution environment
# and produces a process. It does not have to be created via ``spawn`` like above.

# using this model, it should be easy to implement several monadic combinators.
# The most obvious ones would probably be
#
# proc.then (env) -> create_another_process(...)
#
#   for starting the second process after the first has exitted with zero exit status.
#   Remaining input of the first process would be used as input for the second.
#   The output and error streams of both processes would be concatenated respectively.
#   The result of the second process would be the result of the combined process.
#   All in all this would be the equivalent of the `&&` combinator in a unix shell.
#   It would only make sense to implement then in a Promise/A+ conformant way.
#
# proc.pipe (env) -> create_another_process(...)
#
#  This would be the equivalent of the unix shell's `|` combinator.
#  Both processes would run concurrently.
#  The output of the first process would be used as input for the second.
#  The error streams of both processes would be merged/interleaved.
#  The result of the combined process would be available when both processes
#  have exited. It would reflext the exitCode and remainingInput of the second
#  process.

# behaves like "true" on a unix shell
#  - produces no output or error
#  - terminates at once with zero exit status
#  - consumes no input
nop = (env)->
  output: Readable.from []
  error: Readable.from []
  result: Promise.resolve(
    exitStatus:0
    remainingInput: env.input
  )


thru = (env)->
  output: env.input
  error: Readable.from []
  result: Promise.resolve(
    exitStatus:0
    remainingInput: env.input
  )





unit env() # initialize environment using the current process.
  .pipe (env)->
    noop(env)
      .then readOneLine (line)->lines.push line
      .then readOneLine (line)->lines.push line


