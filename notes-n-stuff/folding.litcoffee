Folding is easy for a simple list. It is trivial if we
can do it recursily (i.e. either have proper tail call optimization, or small
enough lists.)

    simple_fold = (arr, v0, reducer)->
      if arrLength is 0 then v0
      else
        [head, tail...] = arr
        v = reducer v0, head
        simple_fold tail, v, reducer

We *could* implement an equivalent operator for streams if

 - we are able to detect the end of the stream end terminate

 - and we are able to deconstruct the stream as we do in the
   simple variant above

Ignoring the problem of detecting termination for a moment,
a naive translation to streams could look something like this

    broken_fold = (readable, v0, reducer)->

      head$ = takeOne readable
      tail$ = dropOne readable

      flatmap head$, (elm)->
        v = reducer v0, elm
        unit broken_fold tail$, v, reducer

As a practical solution, this would not work, because typical ECMAScript
implementation don't do tail call optiization. We can avoid the recursion
by allowing the operator to carry state. I think this is an acceptable
price to pay, since it won't make any difference to the rest of the program.
Also, ignore the whole termination problem for now. Just output a stream
that produces the current accumulator value, starting with the initialization
value and then producing the next value whenever an input item arrives.
BaconJS calls this operator `scan`:


    scan = (readable, v0, reducer)->

      v = undefined

      input = startWith readable, v0
      flatmap input , (elm)->
        unit v = if v? then reducer v, elm else elm


But wait. We have cheated and just implied the existence of another operator.
This may not be as trivial as it seems, since it is actually a rather lowlevel
operation. (It's basically `cons`, only for streams).
So maybe we already have something like this in our arsenal.
Here is an alternative, if we do not have `cons`, but something else that
creats a readable from an Array. We then can simply use `flatmap` again:

    startWith = (readable, value)->
      input = fromArray [ unit(value), readable ]
      flatmap input, identity

So... it seems totally possible to define `scan`.
We could combine `scan` with another, yet missing, operator `last` to create
the actual `fold`.

    fold = (readable, v0, reducer)->last scan readable, v0, reducer

Note how `last` isolates the problem of termination. With a bit of luck,
it will be the only place in our program that has to deal with this
kind of things.

    last = (readable, options={})->
      empty = true
      value = undefined
      read = ->
        self = this
        readable.on "data", (d)->
          value=d
          empty=false
        readable.on "error", (e)->self.destroy e
        readable.on "end", ->
          self.push value unless empty
          self.push null

      new Readable {options..., objectMode: true, read}
