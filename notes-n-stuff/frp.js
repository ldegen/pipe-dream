/* NOTE: This module is not used by the Pipe Dream library.
 * I created it a long time ago to better understand some of the
 * basic flatmap variants.
 */

/* let's create a very basic "observable"
 * without any fancy-smancy.
 *
 * I tried to immitated the way baconjs handles this.
 */
var source = function(create){

  //here we keep a list of subscriptions.
  var subscriptions = [];

  //this is initialized by the create function.
  //When the last subscription is cancled, we
  //call it to allow the underlying data source
  //to free resources if necessary.
  var destroy = null;

  // a flag that is set once an end event was emitted
  var ended = false;

  // this is called by clients to recieve events from this
  // observable. The given handler will be called for each
  // raw event.
  // Event objects will always have at least a property `type`.
  // Its value is either `"next"`,`"end"` or `"error"`.
  // The event may carry an optional property `payload`.
  var subscribe = function(f){
    //if the stream already ended, we need to notify
    //any new subscribes at once. No need for more book keeping in this case.
    if(ended){
      f({type:"end"});
      return function(){};
    }
    //call the create function when the first
    //observer subscribes.
    if(subscriptions.length===0){
      setTimeout(function(){
        destroy = create(emit);
      }, 0);
    }
    subscriptions.push(f);

    //return a function to cancle the subscription.
    return function(){
      subscriptions = subscriptions.filter(function(g){return f!==g;});
      if(subscriptions.length === 0 && typeof destroy === "function"){
        destroy();
      }
    };
  };

  //this is expected to be called within the `create` callback to push
  //an event downstream. `ev` should be an object, and it must at the
  //very least have a property named `type`.
  // Its value must be either `"next"`,`"end"` or `"error"`.
  // Calling this method after an event of type `"end"` has been pushed
  // will raise an exception.
  var emit = function(ev){
    if(ended){
      throw new Error("Attempt to emit an event on an already ended stream.");
    }
    if(typeof ev !== "object"||ev===null||typeof ev.type !== "string"){
      throw new Error("Not a valid event.");
    }
    if(subscriptions.length > 0){
      subscriptions.forEach(function(s){
        s.call(null, ev);
      });
    }
    if(ev.type === "end"){
      subscriptions = [];
      if(typeof destroy === "function"){
        destroy();
      }
      ended = true;
    }
  };

  return {
    subscribe: subscribe,
    map_: function(f){
      return map_(this, f);
    },
    filter_: function(f){
      return filter_(this, f);
    },
    map: function(f){
      return map(this, f);
    },
    filter: function(f){
      return filter(this, f);
    },
    flatmap: function(f){
      return flatmap(this, f);
    },
    take: function(N){
      return take(this, N);
    },
    takeWhile: function(f){
      return takeWhile(this, f);
    },
    record: function(){
      return record(this);
    }
  };

};


/* there are several plausible variants of flatmap.
 *
 * Vanilla flatmap will interleave all events from all streams.
 *
 * flatmap and flatmapConcat will pass on ALL events from ALL streams.
 * flatmapFirst and flatmapLatest will not.
 *
 * flatmapFirst will emit events from the first stream, dropping
 * everything that comes out of the other streams until the first stream
 * ends. It will then switch to the second stream and so on.
 *
 * flatmapLatest will only emit events from the latest stream. Once a new
 * one is created it will switch to that one.
 */


var flatmap = function(observable, f){
  return source(function(emit){
    var unsubscribeList = []
    var activeCount = 1; //start with one because the src stream is active
    unsubscribeList.push(observable.subscribe(function(srcEv){
      //console.log("flatmap: srcEv", srcEv);
      switch(srcEv.type){
      case "error":
        emit(srcEv);
        break;
      case "end":
        activeCount--;
        //console.log("activeCount--", activeCount);
        if(activeCount === 0){
          emit(srcEv);
        }
        break;
      default:
        unsubscribeList.push(f(srcEv.payload).subscribe(function(fEv){
          //console.log("fEv", fEv);
          if(fEv.type==="end"){
            activeCount--;
            //console.log("activeCount--", activeCount);
            if(activeCount === 0){
              emit(fEv);
            }
          }else{
            emit(fEv);
          }
        }));
        activeCount++;
        //console.log("activeCount++", activeCount);
      }
    }));
    return function(){
      unsubscribeList.forEach(function(u){
        u();
      });
    };
  });
};


var unit = function(){
  var args = Array.prototype.slice.call(arguments);
  //console.log("args", args);
  return source(function(emit){
    args.forEach(function(v){
      //console.log("v", v);
      emit({type:"next", payload:v});
    });
    emit({type:"end"});
  });
};

var empty = function(){
  return source(function(emit){
    emit({type:"end"});
  });
};

// a version of map that is not defined in terms of flatmap.
// Maybe useful as an exercise, or for testing.
var map_ = function(src, f){
  return source(function(emit){
    return src.subscribe(function(ev){
      if(ev.type==="next"){
        emit({...ev, payload: f(ev.payload)});
      } else {
        emit(ev);
      }
    });
  });
};

// a version of filter that is not defined in terms of flatmap.
// Maybe useful as an exercise, or for testing.
var filter_ = function(src, f){
  return source(function(emit){
    return src.subscribe(function(ev){
      if(ev.type!=="next" || f(ev.payload)){
        emit(ev);
      }
    });
  });
};

var map = function(src, f){
  return flatmap(src, function(v){
    return unit(v);
  });
};

var filter = function(src, f){
  return flatmap(src, function(v){
    return f(v) ? unit(v) : empty;
  });
};

/* there are a couple of operators that cannot be
 * expressed through flatmap & co.
 * This includes anything that ends the output, before the input has ended.
 */
var takeWhile = function(src, f){
  var i = 0;
  return source(function(emit){
    var unsubscribe = src.subscribe(function(ev){
      if(ev.type==="next"){
        if(f(ev.payload,i++)){
          emit(ev);
        } else {
          unsubscribe();
          emit({type:"end"});
        }
      } else {
        emit(ev);
      }
    });
    return unsubscribe;
  });
};

var take = function(src, N){
  return takeWhile(src, function(_, i){
    //console.log("take: i", i, "N", N);
    return i<N;
  });
};

/* this is useful for debugging/testing
 * It creates a promise that resolves when the stream ends.
 * The resolved value will be list of all events emited by the stream.
 */
var record = function(src){
  var events = [];
  return new Promise(function(resolve){
    var unsubscribe = src.subscribe(function(ev){
      events.push(ev);
      if(ev.type==="end"){
        resolve(events);
        unsubscribe();
      }
    });
  });
};
source.unit = unit;
source.empty = empty;
module.exports = source;
