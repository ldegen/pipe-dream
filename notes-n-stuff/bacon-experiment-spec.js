var Bacon= require('baconjs');

xdescribe("Bacon's flatMap Variants", function(){

  var trace;
  beforeEach(function(){
    trace = [];
  });


  var spawn = function(millis, values){
    return function(src){
      trace.push(src+"_spawn");
      // we could use Bacon.sequentially here. But I would like
      // to trace out the low level details.
      var i = 0;
      return Bacon.fromBinder(function(sink) {
        trace.push(src+"_createSink");
        var intervalId = setInterval(function(){
          if(i<values.length){
            var v = values[i++];
            sink(src+"_"+v);
          } else {
            clear();
            sink(new Bacon.End());
          }
        }, millis);
        var clear = function(){
          clearInterval(intervalId);
        };
        return clear;
      });
    };
  };

  describe("flatMapConcat", function(){
    it("only spawns a new stream after the previous ended, buffering all input events that occur in the meantime", function(){
      var s = Bacon
        .sequentially(10,['a','b','c'])
        .flatMapConcat(spawn(5, [1,2,3]));

      var p = s
        .toPromise()
        .then(function(){
          return trace;
        });


      s.onValue(function(v){
        trace.push(v)

      });
      return expect(p).to.eventually.eql([
        "a_spawn",
        "a_createSink",
        "a_1",
        "a_2",
        "a_3",
        "b_spawn",
        "b_createSink",
        "b_1",
        "b_2",
        "b_3",
        "c_spawn",
        "c_createSink",
        "c_1",
        "c_2",
        "c_3",
      ]);
    });
  });
})



