Well-behaved Node.js streams
============================

Start by observing how ``Readable.pipe()`` is broken:

.. code:: coffee

  # here be some input:

  foo = some_readable ...

  # assume we have two transforms
  bar = new Transform ...
  baz = new Transform ...

  # we can apply them like and produce a new readable
  foo2 = foo
    .pipe bar
    .pipe baz

  # But how to *compose* both Transforms?

  awkward_compose = (a,b)->(readble)->readble.pipe(a).pipe(b)

  foo2 = awkward_compose(bar, baz) foo

Nah... that won't do. ``awkward_compose`` does not return a ``Transform``.
There are a couple of libraries out there that try to do this properly, i.e.
they implement some variant of a ``compose`` function that acts as a binary
operator on ``Transform``. I think there is nothing inherently wrong with that idea,
but I would like to approach the problem differently.

I think we can do without ``Writable`` and/or ``Transform`` streams. After all we want to *transform* a
``Readable`` stream. What if ``Readable`` would behave like a propper (i.e. monadic) stream?
Shouldn't it be enough to add a very small number of very basic operators like `flatmap`
and `scan` to completely cover all our needs?

Pipe Dream, huh?
================

Pipe Dream implements a small set of stream operators that can be used with
plain Node.js ``Readable``\ s:

.. code:: coffee

  {flatmap, fromArray, map} = require "@l.degener/pipe-dream"
  # produce a Readable of chunks -1, 1, -2, 4, -3, 9
  readable = flatmap(fromArray([1,2,3]), (d->fromArray([-d,d*d]))

  # the readable is just a plain Readable, nothing special about it.
  map(readable, d->"#{d}\n").pipe(process.stdout)


Pipe Dream can add a thin layer of syntax sugar on top of this, but this is
completely optional:

.. code:: coffee

  # this does the same as the code above:
  {wrap, fromArray} = require "@l.degener/pipe-dream"

  # note: wrap.fromArray(...) is an alias for wrap(fromArray(...))

  wrap
    .fromArray [1,2,3]
    .flatmap d->fromArray [-d, d*d]
    .map d->"#{d}\n"
    .pipe process.stdout

Note that the values returned by ``wrap.fromArray``, ``wrap.flatmap``, etc. are
**not** ``Readable``\ s. But you can always call ``wrap.readable`` to obtain a
reference to the wrapped ``Readable`` stream.

Object Mode vs Raw Mode
=======================

The operators in this library all work on the assumption that a stream is a
sequence of discrete elements. So ``objectMode: true`` is a natural fit here.
This does not mean that the operators won't work with raw streams. But the
exact behaviour may be difficult to predict. Wherever you are about to do some
serious transformations on your streams, it is probably best to preprocess them
in some way that produces discrete elements in a reliable, deterministic
manner. For instance, if you work with text input, chop it into lines first.
Have a look at the ``chop`` and ``chop.lines`` functions.

And once you are done with your transformations, you might want to convert your
object mode streams back into raw streams (e.g. before piping to stdout). We
also provide utility functions support this.  Have a look at ``join`` and
friends.

