=======================
Variants of ``flatmap``
=======================


``flatmap`` is simple
=====================

... as long as you are dealing with lists.

You have probably already overheard someone mentioning that
streams (or observables) are basically just lists (of potentially
infinite length). So let's imagine we are just talking about lists for a moment.

The ``flatmap`` operator can be seen as a very generic way to transform
lists. If you worked with arrays in Javascript, you are probably familiar
with the ``map`` operator. You could say that ``map`` is a specialization of ``flatmap``.

``map`` allows you to apply an unary function to each element of the list.
It will return a new list that contains the results:

.. code:: coffee

  expect([1,2,3].map (x)->x*2).to.eql [2,4,6]

``flatmap`` does the same, but it expects your function to return a list.
It will return a new list that is the concatenation of all the results:

.. code:: coffee

  expect(flatmap([1,2,3], (x)->[-x,x])).to.eql [-1,1,-2,2,-3,3]

Why is it called "flatmap"? Imagine, you would apply the same function
to the same list using ``map`` instead:

.. code:: coffee

  expect([1,2,3].map (x)->[-x,x]).to.eql [[-1,1],[-2,2],[-3,3]]

You would get a list of lists. You can think of ``flatmap`` as
an operator that "flattens" the the output of an ordinary ``map``.

The ``flatmap`` operator is a very useful operator and I find myself using it all the time.
For arrays, it is very easy to implement. You can do it in a lot of different ways; here is one implementation that uses
``Array::reduce``:

.. code:: coffee

  flatmap = (arr, f)->
    reducer = (out, elm, i, arr)->out.concat f(elm,i,arr)
    arr.reduce reducer, []

It is also worth mentioning that lists (or arrays) together with ``flatmap``
and an the trivial operation ``(x)->[x]`` form a monad. Specifically,
``flatmap`` takes the role of the ``bind``-Operator (``>>=`` in Haskell). So
you could say that ``flatmap`` is to lists what ``.then`` is to Promises: An
elegant way to **compose** simple transformations into complex ones while
maintaining a crucial "self-similarity".  At every level you are taking a value
out of some container and transform it into a new Container of the same type. Like a list, or a
Promise, or a Maybe, or a Stream, or a Program or whatever monadic type you use
to model your system.

``flatmap`` on streams
======================

Since streams are essentially lists, we expect ``flatmap`` to roughly work the
same here.  And it does. But there is a catch: Time.  When you think about a
list -- ignore lazy lists for a sake of this argument -- you basically assume
that the length of the list is a) finite and b) an a priory fact.  However you
cannot be sure of either when dealing with streams.

So suppose we run ``flatmap`` on a stream of elements ``[a, b, c, ...]``,
applying some transformation ``f``.  Now, for the first input element ``a`` we
create another stream ``f a``. Ultimately, we will have to merge the elements
from this stream into our output stream. But it could be that the input
elements ``b`` and ``c`` "arrive" long before ``f a`` is finished (or has event
started to produce elements). So what should we do?  Wait for ``f a`` to finish
(which may be never)? How long should we keep buffering input elements?  Or
should we just process any input element as it arrives? This would mess up the
order of output elements, but that may not be a problem.  But how long should
we keep waiting for elements from ``f a``?  How many streams should we keep
"listening" to? Remember that *none* of the infolved streams is guaranteed to
be finite.

To these questions, there is no single right answer. Most FRP libraries implement
more than one variation. It is up to the application developer to pick the one
that best fits the particular use case. In essence, it boils down to three questions

- What is the maximum number of streams ``flatmap`` should spawn concurrently?

- What should ``flatmap`` do once that limit is reached and another input element arrives?

  - Ignore/discard the element. ``flatmapFirst`` is a specail case of this behaviour with
    a concurrency limit of 1.

  - Stop listening to one of the streams it is currently listening to and spawn an new stream for the new input element.
    ``flatmapLatest`` is a special case with concurrency of 1. Buffering is

  - Buffer the element until one of the active streams has ended. ``flatmapConcat`` is a special case of this
    behaviour with concurrency limit of 1.

Concurrency Limit
-----------------

This controls the number of streams flatmap will keep listening to at any time. If there is no limit, you get
the naive "vanilla" flatmap that simply tries to catch and process all elements as they arrive.
Disable it only if you know that the streams that are spawned are short-lived (in relation to the frequency
of input events).

Buffering and Backpressure
--------------------------

``flatmap`` will not buffer input itself. But it is of course build from Node.js streams and those will
always apply buffering. Under the hood, ``flatmap`` uses a custom ``Writable`` stream called ``inlet`` and
a ``PassThrough`` stream ``sink``. The input is piped to the ``inlet``. For each element, the ``inlet``
will call the provided transformation yielding a ``Readable``. Those readables are called ``sources``.
Each ``source`` is piped to the sink. Buffering inside of ``flatmap`` can occur at two places:

- when more elements arrive via the ``sources`` than can be consumed from the ``sink``, the buffer of ``sink``
  will start to fill, eventually excerting backpressure on the ``sources`` once the ``sink``'s high-water mark is exceeded.

- when more elements arrive via the ``input`` than can be processed by the ``inlet``, the buffer of ``sink``
  will start to fill, eventually excerting backpressure on the ``input`` once the ``input``'s high-water mark is exceeded.

Wether we apply a concurency limit and how we enforce it has a huge impact on how much buffering is required.
Once the limit is violated, the ``inlet`` will stop consuming input until it is satisfied again, which typically
causes the input that arrives in the meantime to get buffered. On the other hand, if you do *not*
limit concurrency, you may be accumulating a large number of ``sources`` which may cause problems
aswell. Note that ``flatmapLatest`` or ``flatmapFirst`` do not have those problems.



Implemented ``flatmap`` Variants
================================

Naive ``flatmap``
-----------------



